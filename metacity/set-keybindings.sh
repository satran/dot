#!/bin/sh
gsettings set org.gnome.desktop.wm.preferences focus-mode sloppy
gsettings set org.gnome.desktop.wm.preferences resize-with-right-button true
gsettings set org.gnome.desktop.wm.preferences button-layout ':close'
gsettings set org.gnome.desktop.wm.keybindings maximize-vertically "['<Super>v']" 
gsettings set org.gnome.desktop.wm.keybindings move-to-center "['<Super>c']"
gsettings set org.gnome.desktop.wm.keybindings move-to-side-e "['<Super>l']"
gsettings set org.gnome.desktop.wm.keybindings move-to-side-n "['<Super>k']"
gsettings set org.gnome.desktop.wm.keybindings move-to-side-s "['<Super>j']"
gsettings set org.gnome.desktop.wm.keybindings move-to-side-w "['<Super>h']"
gsettings set org.gnome.mutter.keybindings toggle-tiled-left "['<Super><Shift>h']"
gsettings set org.gnome.mutter.keybindings toggle-tiled-right "['<Super><Shift>l']"
gsettings set org.gnome.desktop.wm.keybindings begin-resize "['<Super>r']"
gsettings set org.gnome.desktop.interface scaling-factor 1
