;; Disable GUI elements that I don't use
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; Set margins
(setq-default left-margin-width 1)
(setq-default right-margin-width 1)

(global-font-lock-mode t)

;; Optimization
;; Set the number of bytes of consing before garbage collection
;; Small hack to make the init time a bit faster
(setq gc-cons-threshold 100000000)

;; Load all the installed packages
(let ((default-directory "~/.emacs.d/"))
  (normal-top-level-add-subdirs-to-load-path))
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(require 'package)
(setq package-archives
      '(("gnu" . "https://elpa.gnu.org/packages/")
        ("melpa" . "https://melpa.org/packages/")))
(package-initialize)

;; Load use-package
(require 'use-package)

;; Cycle between a dark and light theme
(setq dark-theme 'plain)
(setq light-theme 'plain-white)

(defun flip-color ()
  "Flip the color theme"
  (interactive)
  (disable-theme current-theme)
  (setq current-theme
        (if (equal current-theme dark-theme) light-theme dark-theme))
  (load-theme current-theme t))

;; Change the theme based on time of the day
;; 6am - 5pm light otherwise dark
(defun get-theme-for-current-time ()
  (let ((hour (nth 2 (decode-time (current-time)))))
    (if (member hour (number-sequence 6 17))
	light-theme dark-theme)))

(defun synchronize-theme ()
  (interactive)
  (let ((theme (get-theme-for-current-time)))
    (setq current-theme theme)
    (load-theme theme t)))

(synchronize-theme)

;; Environments
(setenv "GOROOT" "/usr/local/go")
(setenv "EDITOR" "emacs")
(setenv "VISUAL" "emacs")
(setenv "PATH" (concat (getenv "PATH") ":" (concat (getenv "HOME") "/bin")))
(setenv "PATH" (concat (getenv "PATH") ":" "/usr/local/go/bin"))
(setenv "PATH" (concat (getenv "PATH") ":" "/usr/local/bin"))
(setenv "GOPATH" (if (getenv "GOPATH") (getenv "GOPATH") (getenv "HOME")))
(setq exec-path (append exec-path '("/usr/local/go/bin" "/home/satran/bin")))

;; Enable default disabled options
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)

;; Have those awesome matching pairs
(electric-pair-mode t)

;; Highlight matching parenthesis
(show-paren-mode 1)

;; Settings for enforcing to use UNIX endlines
(set-default-coding-systems 'utf-8-unix)
(prefer-coding-system 'utf-8-unix)

;; Set such that emacs does not use the ugly word-wrapping
(global-visual-line-mode 1)

;; Use vertical splitting more often than horizontal
(setq split-height-threshold 200)

;; C-v and M-v don't undo each other, because the point position isn't
;; preserved. Fix that.
(setq scroll-preserve-screen-position 'always)

;; Do not pollute the working directory. Add it to emacs folder.
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq delete-old-versions -1)
(setq version-control t)
(setq vc-make-backup-files t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))

;; Save history
(setq savehist-file "~/.emacs.d/savehist")
(savehist-mode 1)
(setq history-length t)
(setq history-delete-duplicates t)
(setq savehist-save-minibuffer-history 1)
(setq savehist-additional-variables
      '(kill-ring
        search-ring
        regexp-search-ring))

;; When you start typing and text is selected, replace it with what
;; you are typing, or pasting, or whatever.
(delete-selection-mode 1)

;; Change typing yes to y and no to n in minibuffer
(fset 'yes-or-no-p 'y-or-n-p)

;; Disable beep when doing incorrect actions
(setq ring-bell-function 'ignore)

;; Set the frame title to buffer name
(setq frame-title-format "%b")

;; Use gtk tooltips when available
(setq x-gtk-use-system-tooltips t)

;; Set columns
(column-number-mode 1)

;; show line number in status bar
(line-number-mode 1)

;; Disable splash screen
(setq inhibit-startup-message t)

;; Set the cursor to a bar
(setq-default cursor-type 'bar)

;; Highlighting current line
(global-hl-line-mode t)

;; Set line height
(setq-default line-spacing 1)
;; I don't like bold fonts and this disables all bold fonts. Ensure to
;; call it after every theme load though.
;; https://stackoverflow.com/questions/2064904/how-to-disable-bold-font-weight-globally-in-emacs#2065305
(defun disable-bold-fonts ()
  (interactive)
  (mapc
   (lambda (face)
     (set-face-attribute face nil :weight 'normal :underline nil))
   (face-list)))

;;(disable-bold-fonts)

;;(defvar-local default-font "iA Writer Duospace-12.5")
;;(defvar-local default-font "-*-lucidatypewriter-medium-*-*-*-14-*-*-*-*-*-iso8859-*")
;;(defvar-local default-font "-*-terminus-medium-*-*-*-*-120-*-*-*-*-*-*")
;;(defvar-local default-font "-*-unifont-medium-*-*-*-*-*-*-*-*-*-*-*")
;;(set-face-attribute 'default nil :font default-font)
;;(set-frame-font default-font nil t)
;;(add-to-list 'default-frame-alist `(font . ,default-font))

(use-package go-eldoc)
(use-package go-mode
  :config
  (setq gofmt-command "goimports")
  (add-hook 'before-save-hook 'gofmt-before-save)
  (add-hook 'go-mode-hook 'flycheck-mode)
  (add-hook 'go-mode-hook 'go-eldoc-setup)
  (add-hook 'go-mode-hook
      (lambda ()
        (set (make-local-variable 'company-backends) '(company-go))
        (company-mode)))
  (add-hook 'go-mode-hook (lambda ()
                            (local-set-key (kbd "M-.") 'godef-jump)))
  (add-hook 'go-mode-hook (lambda ()
                            (local-set-key (kbd "M-,") 'godef-jump-other-window))))

;; Lisp
(use-package paredit
  :config
  (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t))

(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)

;; EWW
(setq eww-search-prefix "https://google.com/search?q=")

;; Ibuffer
(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-auto-mode 1)
            (ibuffer-switch-to-saved-filter-groups "default")))

(setq ibuffer-default-sorting-mode (quote alphabetic))
(setq ibuffer-formats
   (quote
    ((mark " "
	   (name))
     (mark " "
	   (name 16 -1)
	   " " filename)
     (mark modified read-only " "
	   (name 18 18 :left :elide)
	   " "
	   (size 9 -1 :right)
	   " "
	   (mode 16 16 :left :elide)
	   " " filename-and-process))))

;; Don't show filter groups if there are no buffers in that group
(setq ibuffer-show-empty-filter-groups nil)

;; Don't ask for confirmation to delete marked buffers
(setq ibuffer-expert t)

;; Shell
(add-hook 'term-mode-hook
          (lambda () (setq-local global-hl-line-mode nil)))
(add-hook 'term-mode-hook
          (lambda () (font-lock-mode t)))

(add-hook 'shell-mode-hook
          (lambda ()
            (font-lock-mode t)
            (local-set-key (kbd "M-n") 'comint-next-matching-input-from-input)))
(add-hook 'shell-mode-hook
          (lambda () (local-set-key (kbd "M-p") 'comint-previous-matching-input-from-input)))

(defvar name-counter 0)

(defun new-shell-name ()
  (setq name-counter (+ 1 name-counter))
  (concat "shell-" (number-to-string name-counter)))

(defun new-shell ()
  (interactive)
  (shell (new-shell-name)))

(global-set-key (kbd "C-S-z") 'new-shell)

(defvar previous-command nil)

(defun replace-with-shell-output ()
  (interactive)
  (let ((cmd (read-shell-command "Shell command on region: " previous-command))
        (buffer (current-buffer)))
    (setq previous-command cmd)
    (shell-command-on-region
     (region-beginning) (region-end) cmd buffer t)))

(global-set-key (kbd "<M-return>") 'replace-with-shell-output)

(use-package ivy
  :init (ivy-mode 1)
  :config
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t)
  (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
  ;; Imitate C-s C-w with the normal isearch
  ;;(define-key minibuffer-local-map (kbd "C-w") 'ivy-yank-word)
  :bind (("C-s" . swiper)
	 ("C-c C-r" . ivy-resume)
	 ("C-x C-f" . counsel-find-file)
	 ("C-c C-f" . counsel-git)
	 ("C-c C-s" . counsel-ag)
	 ("C-c j" . counsel-git-grep)
	 ("C-x l" . counsel-locate)))

;; Global Keybindings
(global-set-key (kbd "C-z") 'shell)
(global-set-key (kbd "C-M-z") 'ansi-term)
(global-set-key (kbd "C-x g") 'magit-status)
;; More reasonable next windows
(global-set-key "\C-x\C-n" 'next-multiframe-window)
(global-set-key "\C-x\C-p" 'previous-multiframe-window)
(global-set-key (kbd "C-x n") 'next-multiframe-window)
(global-set-key (kbd "C-x p") 'previous-multiframe-window)
(global-set-key (kbd "C-x C-b") 'ibuffer)
;; Move across split windows using the shit+arrow keys
(windmove-default-keybindings)

;; Functions

;; A lot of times I have tried quitting emacs and then realized that I
;; forgot to do something to a buffer. This is to fix it. Source
;; [[http://trey-jackson.blogspot.de/2010/04/emacs-tip-36-abort-minibuffer-when.html][here]]

(defun stop-using-minibuffer ()
  "kill the minibuffer"
  (when (and (>= (recursion-depth) 1) (active-minibuffer-window))
    (abort-recursive-edit)))

(add-hook 'mouse-leave-buffer-hook 'stop-using-minibuffer)

(defun sb/uplat ()
  (interactive)
  (find-file "~/src/github.solarisbank.de/utility-platform"))

;; Increase and decrease frame size horizontally, from https://www.emacswiki.org/emacs/frame-cmds.el
(defun shrink-frame-horizontally (&optional increment frame) ; Suggested binding: `C-M-left'.
  "Decrease the width of FRAME (default: selected-frame) by INCREMENT.
INCREMENT is in columns (characters).
Interactively, it is given by the prefix argument."
  (interactive "p")
  (set-frame-width frame (- (frame-width frame) increment)))

(defun enlarge-frame-horizontally (&optional increment frame) ; Suggested binding: `C-M-right'.
  "Increase the width of FRAME (default: selected-frame) by INCREMENT.
INCREMENT is in columns (characters).
Interactively, it is given by the prefix argument."
  (interactive "p")
  (set-frame-width frame (+ (frame-width frame) increment)))

(defun shrink-frame-vertically (&optional increment frame) ; Suggested binding: `C-M-left'.
  "Decrease the height of FRAME (default: selected-frame) by INCREMENT.
INCREMENT is in columns (characters).
Interactively, it is given by the prefix argument."
  (interactive "p")
  (set-frame-height frame (- (frame-height frame) increment)))

(defun enlarge-frame-vertically (&optional increment frame) ; Suggested binding: `C-M-right'.
  "Increase the height of FRAME (default: selected-frame) by INCREMENT.
INCREMENT is in columns (characters).
Interactively, it is given by the prefix argument."
  (interactive "p")
  (set-frame-height frame (+ (frame-height frame) increment)))

(global-set-key (kbd "M-<left>") 'shrink-frame-horizontally)
(global-set-key (kbd "M-<right>") 'enlarge-frame-horizontally)
(global-set-key (kbd "M-<up>") 'shrink-frame-vertically)
(global-set-key (kbd "M-<down>") 'enlarge-frame-vertically)

;; Disable dired junk
(use-package dired-details
    :config (dired-details-install)
    (setq dired-details-hidden-string " "))

(use-package plumb-mode)

;; Remove space between frame and corners in fullscreen
(setq frame-resize-pixelwise t)

;; Hydra Keybindings
(global-set-key (kbd "C-;") 'hydra-global/body)
(global-set-key (kbd "M-;") 'hydra-global/body)
(defhydra hydra-global (:color blue :hint nil)
  "
_f_: files	_w_: windows    _g_: magit status
_b_: buffers	_l_: looks      _c_: capture
_o_: orgmode	_t_: text       _m_: notmuch
"
  ("b" hydra-local-buffers/body :quit t)
  ("c" (find-file "/Users/satyajitranjeev/Library/Mobile Documents/27N4MQEA55~pro~writer/Documents/log.md") :quit t)
  ("f" hydra-local-files/body :quit t)
  ("g" magit-status :quit t)
  ("l" hydra-local-looks/body :quit t)
  ("o" hydra-local-org/body :quit t)
  ("m" notmuch :quit t)
  ("s" hydra-local-search/body :quit t)
  ("t" (find-file "~/org/log.txt") :quit t)
  ("w" hydra-local-windows/body :quit t))

(defun switch-to-ibuffer ()
  (interactive)
  (switch-to-buffer-other-frame (ibuffer-sidebar-buffer)))

(defhydra hydra-local-buffers (:color red :hint nil)
  "
_b_: list ibuffer
_f_: focus ibuffer sidebar
_p_: previous buffer
_n_: next buffer
"
  ("b" ibuffer-sidebar-toggle-sidebar :quit t)
  ("f" switch-to-ibuffer :quit t)
  ("p" previous-buffer :quit t)
  ("n" next-buffer :quit t))

(defhydra hydra-local-files (:color red :hint nil)
  "
_f_: open file
_t_: toggle sidebar
_u_: uplat directory
_g_: hydra open via git
"
  ("f" find-file :quit t)
  ("t" dired-sidebar-toggle-sidebar :quit t)
  ("u" sb/uplat :quit t)
  ("g" counsel-git :quit t))

(defhydra hydra-local-windows (:color red :hint nil)
  "
_m_: maximize window
_h_: split frame horizontally
_v_: split frame vertically
_n_: new frame
"
  ("m" delete-other-windows :quit t)
  ("h" split-window-right :quit t)
  ("v" split-window-below :quit t)
  ("n" make-frame-command :quit t))

(defhydra hydra-local-search (:color red :hint nil)
  "
_g_: git grep
"
  ("g" counsel-git-grep :quit t))

(defhydra hydra-local-looks (:color red :hint nil)
  "
_t_: load theme            _f_: flip color
_T_: disable theme         _h_: toggle global highlight
_l_: toggle truncate lines _m_: toggle mode line
_w_: enable window margin  _W_: disable window margin
_d_: toggle frame decorations
"
  ("d" (if (frame-parameter nil 'undecorated)
	   (set-frame-parameter nil 'undecorated nil)
	 (set-frame-parameter nil 'undecorated t)))
  ("f" flip-color :quit t)
  ("h" global-font-lock-mode :quit t)
  ("l" toggle-truncate-lines :quit t)
  ("m" hide-mode-line-mode :quit t)
  ("t" load-theme :quit t)
  ("T" disable-theme :quit t)
  ("w" (set-window-margins nil 5 5))
  ("W" (set-window-margins nil 0 0)))

(defhydra hydra-local-org (:color red :hint nil)
  "
_n_: narrow to subtree    _p_: narrow to page
"
  ("n" org-narrow-to-subtree :quit t)
  ("p" narrow-to-page :quit t))

(defun xterm-title-update ()
  (interactive)
  (send-string-to-terminal (concat "\033]1; " (buffer-name) "\007"))
  (send-string-to-terminal (concat "\033]2; " (buffer-name) "\007")))

(if (not (display-graphic-p))
    (add-hook 'post-command-hook 'xterm-title-update))

;; Set the window border to dark theme for adwaita
;; http://www.whiz.se/2016/05/01/dark-theme-in-emacs/
(defun set-dark-wm-theme (frame)
  (select-frame frame) ;; this is important!
  (when (display-graphic-p)
    (progn
      (when (file-exists-p "/usr/bin/xprop")
    (progn
      (defvar winid nil)
      (setq winid (frame-parameter frame 'outer-window-id))
      (call-process "xprop" nil nil nil "-f" "_GTK_THEME_VARIANT" "8u" "-set" "_GTK_THEME_VARIANT" "dark" "-id" winid))))))

(defun on-after-init ()
  (set-dark-wm-theme (selected-frame))
  (unless (display-graphic-p (selected-frame))
    (set-face-background 'default "unspecified-bg" (selected-frame))))

(if (display-graphic-p)
    (progn
      (add-hook 'window-setup-hook 'on-after-init)
      (add-hook 'after-make-frame-functions 'set-dark-wm-theme)))



(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector [])
 '(markdown-hide-markup t)
 '(markdown-hide-urls t)
 '(markdown-list-item-bullets (quote ("•" "•" "•" "•" "•" "•" "•")))
 '(neo-theme (quote nerd))
 '(package-selected-packages
   (quote
    (hide-mode-line plantuml-mode neotree company pdf-tools vala-mode solarized-theme writeroom-mode markdown-mode autothemer magit hydra counsel swiper ivy paredit flycheck go-eldoc go-mode use-package))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fringe ((t (:inherit default))))
 '(highlight ((t (:background "#1a4b77" :foreground "white" :underline nil :weight normal))))
 '(ivy-subdir ((t (:inherit default))))
 '(region ((t (:background "#1a4b77" :foreground "white" :underline nil :weight normal)))))
