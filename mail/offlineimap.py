#! /usr/bin/env python2
from subprocess import check_output

def get_pass_ranjeev():
    return check_output("pass email/s@ranjeev.in", shell=True).strip("\n")
