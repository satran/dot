#!/bin/sh
light=$1
if [ -z "$light" ]; then
	light=4500
fi
gsettings set org.gnome.settings-daemon.plugins.color night-light-temperature $light
