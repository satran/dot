# Dont accidently delete something
# For linux machines.
function rm() { 
    mv "$@" ~/.local/share/Trash/files/ ;
}

# Shortcut for ps and grep
function psgrep() {
    ps aux | grep -v grep | grep "$@" -i --color=auto;
}

# Shortcut for histroy and grep
function hisgrep() { 
    history | grep -v grep | grep "$@" -i --color=auto;
}

function rmt() {
    find . -iname "*~" -exec rm "{}" \; 
    find . -iname "#*" -exec rm "{}" \; 
}
