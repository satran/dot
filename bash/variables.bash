export EDITOR='emacsclient -c'
export VISUAL='emacsclient -c'
#export TERM=xterm-256color

# Record each line as it gets issued
export PROMPT_COMMAND='history -a'

# Huge history. Doesn't appear to slow things down, so why not?
export HISTSIZE=-1
export HISTFILESIZE=-1

# Avoid duplicate entries
export HISTCONTROL="erasedups:ignoreboth"

# Don't record some commands
export HISTIGNORE="&:[ ]*:exit:ls:bg:fg:history"

if [[ -z "$cluster_name" ]]; then
    PS1='\W $(__git_ps1 "(%s)")\$ '
else
    PS1='$cluster_name | \W $(__git_ps1 "(%s)")\$ '
fi
