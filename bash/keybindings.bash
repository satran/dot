if [ ! -n "$INSIDE_EMACS" ]; then
    bind '"\e[A": history-search-backward'
    bind '"\e[B": history-search-forward'
    bind '"\C-P": history-search-backward'
    bind '"\C-N": history-search-forward'
fi
