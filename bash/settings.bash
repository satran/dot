# Append to the history file, don't overwrite it
shopt -s histappend
# Save multi-line commands as one command
shopt -s cmdhist
# Disable Ctrl-s permantly in the terminal
# Just in case you have forgotten how to quit use Ctrl-q
stty -ixon

[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion
