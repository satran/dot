# Prompt settings for git branch
if [[ -f "/usr/share/git-core/contrib/completion/git-prompt.sh" ]]; then
    source /usr/share/git-core/contrib/completion/git-prompt.sh
elif [[ -f "/usr/share/git/git-prompt.sh" ]]; then
    source /usr/share/git/git-prompt.sh
else
    source $DOT/git/git-prompt.sh
fi


# Autocompletion
if [[ -f "/usr/share/git/completion/git-completion.bash" ]]; then
    source /usr/share/git/completion/git-completion.bash
fi
