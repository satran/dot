(in-package :stumpwm)

(undefine-key *root-map* (kbd "c"))
(undefine-key *root-map* (kbd "C-c"))
(define-key *root-map* (kbd "c") "exec terminator")
(define-key *root-map* (kbd "C-c") "exec terminatore")
(undefine-key *root-map* (kbd "e"))
(define-key *root-map* (kbd "e") "exec emacsclient -c")
(define-key *root-map* (kbd "C-e") "exec emacs")
(undefine-key *root-map* (kbd "f"))
(define-key *root-map* (kbd "f") "exec firefox")
(define-key *root-map* (kbd "C-f") "exec firefox")
(define-key *root-map* (kbd "C-TAB") "fnext")
(define-key *root-map* (kbd "C-o") "other-window")
(define-key *root-map* (kbd "C-[") "move-window left")
(define-key *root-map* (kbd "C-]") "move-window right")

;; Message window font
(set-font "-*-terminus-medium-r-*-*-20-*-*-*-*-*-*-*")

(setf *mouse-focus-policy* :sloppy) ;; :click, :ignore, :sloppy

;;(setq *maxsize-border-width* 0)
(setq *transient-border-width* 1)
(setq *window-border-style* :thin)
(set-focus-color "#000000")
(set-unfocus-color "#000000")
(set-float-focus-color "#000000")
(set-float-unfocus-color "#000000")

(set-normal-gravity :center)

(defvar *battery-status-command*
  "acpi -b | awk -F '[ ,]' '{printf \"%s%s\", $3, $5}' | sed s/Discharging/\-/ | sed s/Unknown// | sed s/Full// | sed s/Charging/+/")

(setf *screen-mode-line-format*
      (list "[^B%n^b] %W^>"
          '(:eval (run-shell-command *battery-status-command* t))
          " | %d"))
(setf *mode-line-position* :bottom)

(setf *window-format* "%m%n%s%c")

(setf *mode-line-timeout* 1)

(toggle-mode-line (current-screen) (current-head))

;; Define the volume control and mute keys.
(define-key *top-map* (kbd "XF86AudioLowerVolume") "exec amixer set Master 5%-")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "exec amixer set Master 5%+")
(define-key *top-map* (kbd "XF86AudioMute") "exec amixer set Master toggle")
(define-key *top-map* (kbd "XF86MonBrightnessDown") "exec xbacklight -dec 10")
(define-key *top-map* (kbd "XF86MonBrightnessUp") "exec xbacklight -inc 10")

;; True type fonts
(add-to-load-path "/home/satran/src/stumpwm/contrib/util/ttf-fonts")
(load-module "ttf-fonts")
(set-font (make-instance 'xft:font :family "DejaVu Sans" :subfamily "Book" :size 13))

;; GAPS! GAPS! GAPS!
(add-to-load-path "/home/satran/src/stumpwm/contrib/util/swm-gaps")
(load-module "swm-gaps")
(setf swm-gaps:*inner-gaps-size* 5)
(setf swm-gaps:*outer-gaps-size* 0)

;; pinentry for gpg
(add-to-load-path "/home/satran/src/stumpwm/contrib/util/pinentry")
(load-module "pinentry")
